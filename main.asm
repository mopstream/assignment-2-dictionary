%define BUFFER_SIZE 255
%define POINTER_SIZE 8


section .rodata
	error_msg1: db "Buffer overflow", 0
	error_msg2: db "Key is not found", 0
	
section .bss
	buffer: resb BUFFER_SIZE

section .text
	
%include "colon.inc"	
%include "words.inc"
%include "lib.inc"
	
global _start

_start:
	mov rdi, buffer		;аргументы для read_word
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax		;ошибка переполнения буфера
	je .err1
	
	mov rdi, buffer		;аргументы для find_word
	mov rsi, NEXT
	push rdx		;сохраняем длину key
	call find_word
	pop rdx
	cmp rax, 0		;слово не найдено
	je .err2

	add rax, POINTER_SIZE	;смещаем указатель с NEXT на key
	add rax, rdx		;смещаем указатель с key на value
	inc rax			;read_word не посчитал 0-терминатор
	mov rdi, rax		;аргумент для print_string
	
	call print_string
	
	.end:
		call print_newline
		mov rdi, 0
		call exit
	.badend:
		call print_newline
		mov rdi, 1
		call exit
	.err1:
		mov rdi, error_msg1
		call print_err
		jmp .badend
	.err2:                     
	        mov rdi, error_msg2
	        call print_err     
	        jmp .badend
