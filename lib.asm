%define zero_code 0x30
%define newline_code 0xA
%define space_code 0x20
%define tab_code 0x9


global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err	
	
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall
	
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .count
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
	ret

print_err:
	push rdi          
	call string_length
	pop rsi           
	mov rdx, rax      
	mov rax, 1        
	mov rdi, 2        
	syscall           
	ret
	
; Принимает код символа и выводит его в stdout
print_char:		
	mov rax, 1
    push rdi
	mov rsi, rsp		;в rsp адрес кода символа
	pop rdi
	mov rdi, 1
	mov rdx, 1
	syscall           
	ret       

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, newline_code
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:            
        mov rdx, rdi   
	xor rcx, rcx		;rcx - счетчик цифр
	dec rsp
	mov byte[rsp], 0
	.loop:	       
            dec rsp		;на стеке по байту буду хранить цифры
	    inc rcx
            mov rax, rdx	;делю RDX:RAX / r11=10 
            xor rdx, rdx
            mov r11, 10
            div r11    
            add rdx, zero_code		; цифра ==> 'цифра'
            mov [rsp], dl
            mov rdx, rax
            cmp rdx, 0		;если при делении осталась целая часть = 0 - конец цикла
            jne .loop  
                       
        mov rdi, rsp   		;аргумент print_string - адрес начала числа
	push rcx		;сохраняю счетчик
	call print_string
        pop rcx	       
	add rsp, rcx   		;возвращаю rsp в начальное положение
	inc rsp	       		;удаляю терминатор из стека
	ret	       
                       
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jnl print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax              
	.loop:				;сравниваю поэлементно строки, конец - совпадение терминаторов
	    mov dl, byte [rsi+rax]
	    cmp byte [rdi+rax], dl 
	    jne .false
        cmp byte [rdi+rax], 0
	    je .true
	    inc rax               
	    jmp .loop            
	.false:
            xor rax, rax
	    ret
        .true:
	    mov rax, 1
	    ret	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	dec rsp
	mov byte[rsp], 0
	mov rdx, 1
	xor rdi, rdi
	mov rsi, rsp
	syscall
	mov al, byte[rsp]
	inc rsp
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:                      
    xor rax, rax            ;текущий прочитанный символ
    xor rcx, rcx		;счетчик символов                                  
    .read:
        cmp rcx, rsi
	jge .badend
        push rcx            ;цикл считывания символов слова
        push rdi            
        push rsi            
        call read_char      
        pop rsi             
        pop rdi             
        pop rcx 
	cmp al, space_code
	je .white
	cmp al, tab_code  
	je .white
	cmp al, newline_code  
	je .white
	mov byte[rdi + rcx], al
	cmp al, 0          ;0-конец потока, записали его как терминатор
	je .success
        inc rcx
	jmp .read

    .badend:
        xor rax, rax        
        ret

    .white:
	cmp rcx, 0
	je .read

    .success:
	mov rdx, rcx
        mov rax, rdi
        ret 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax		;число
	xor rdx, rdx		;длина
	xor rcx, rcx		;буфер для считывания

	.num:
	    xor rcx, rcx
  	    mov cl, [rdi+rdx]	;чтение цифры
	    cmp cl, zero_code		;проверка на попадание в диапазон [0-9]
	    jb .end 
	    cmp cl, '9'     
	    ja .end
	    sub rcx, zero_code		;'цифра' ==> цифра
	    mov r11, 10		;умножаем число на 10
	    push rdx
	    mul r11
	    pop rdx
	    add rax, rcx	;rax = 10*rax + rcx - новое число
	    inc rdx
	    jmp .num
	
	.end:
 	    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '+'	;если первый символ - '+' или '-', пропускаем его и парсим число со второго символа
	je .parser
	cmp byte[rdi], '-'
	jne parse_uint	  	;иначе парсим с первого
	.parser:
	    push rdi
	    inc rdi		  
	    call parse_uint	  
	    pop rdi
	    inc rdx
	    cmp byte[rdi], '+'	;еще одна проверка знака
	    je .rret
	    neg rax		  
	    .rret:
		ret
	

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi		;сравнение длины строки и буфера
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi	
	cmp rdx, rax
	jb .err

	xor rcx, rcx		;запись
	.loop:
	    mov r11, [rdi + rcx]
	    mov [rsi+rcx], r11
	    inc rcx
	    cmp rax, rcx
	    jbe .end
	    jmp .loop

	.end:
	    ret

	.err:
	    xor rax, rax
	    ret
