%define POINTER_SIZE 8	

	
extern string_equals
global find_word
	

	
find_word:
	push rsi
	add rsi, POINTER_SIZE	;Смещаем указатель с указателя NEXT на сам key
	push rdi		;Сохраняем регистры
	call string_equals	;Проверяем равенство слов
	pop rdi
	pop rsi
	
	cmp rax, 1		;Если строки равны -- успех
	je .success

	mov rsi, [rsi]		;Смещаем указатель на следующий элемент списка
	cmp rsi, 0		;Если он есть -- снова проверяем слово
	jne find_word

	xor rax, rax		;Иначе -- 0
	ret

	.success:
		mov rax, rsi	;Возвращаем указатель на найденный элемент словаря
		ret
